﻿﻿## Cabify shop challenge

### Basic instructions:

Run project:
```bash
yarn start
```

Run unit test:
```bash
yarn test
```

### About the project:

#### Why React?

I selected this stack because:

- React is being kept by Facebook, and regularly they publish new features. 
-  React has a wide community, where you can easily find help and many useful libraries.
- React is a stable project used by many companies.

__Why React and not Vue?__

Vue and React have similar features. Vue was inspired by React and Angular, so it should be better, shuldn’t it?
I prefer React because:

- React is not a strict framework, so you can select your own workflow (Although it could be a double-edged sword).
- JavaScript is more present in the development. 
- Nowadays, we can find more variety in libraries.
- Although you can choose you flow method, React promotes an immutable philosophy.

__Why Redux vs Mobx?__

I have never worked with Mobx but I have heard about it. I would like to try it in an app and test how it works, but in this challenge I decided to use Redux:

- Redux is immutable, which is very important in order to maintain a right data structure.
- It has a wide community.
- There are some useful libraries compatibles with Redux, like old Redux form or new Form Nerd.

### Main features:

__Sass structure__:

I decided to use the provided CSS file to create my custom SASS structure based on 7-in-1 pattern and BEM pattern.
I took this decision because creating a SASS structure would not take too much time and it would be easier to maintain.

__Translations and currencies:__

A virtual shop is and application where many different people access, so is important to have the right translations and currency system.
It is best to implement this philosophy when the project is at its early stages, before it gets too complex to do it.

__Testing:__

Testing is also a very important feature to employ in all apps. Currently, I have done some unit test, and even though I am not an expert QA developer, it is one of the skills I am working on.


__Components:__

My objective was to reduce app functionality into small reusable components, easily to transform and maintain. Each component pending on controls itself. Nevertheless, I decided to connect some components with the store, because there are focused on working with this app.

__Store:__

We can find some workflows with Redux, and sometimes is few difficult to select which is better to us.
I decided to work with [reduxsauce](https://github.com/jkeam/reduxsauce#readme), a library that provides tools to manage easily and clearly our store.

In addition, I thought a persistent store in a shop app was required. We would not want to lose our cart items if we refresh the page, so I am using redux-persist library to avoid it.

Also, I am using devTools plugin to have the ability to check our store in browsers.

### Extra

- I made a modal to show product details.
- And decided to implement a responsive design.
