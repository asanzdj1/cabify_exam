import { getFormattedPrice } from 'utils/money';

class Checkout {
  _items = {};
  _pricingRules = [];
  _products = [];

  /*
   * Items shape:
   * id: {
   *   quantity,
   *   price,
   * }
   * */

  constructor(pricingRules = [], products = []) {
    this._pricingRules = pricingRules;
    this._products = products;
  }

  scan = (productId, quantity = 1) => {
    const product =
      this._products && this._products.find(prod => prod.id === productId);

    if (!product) return;

    this._items = {
      ...this._items,
      [productId]: {
        quantity: parseFloat(quantity),
        price: parseFloat(product.price),
      },
    };
  };

  setProducts = products => {
    this._products = products;
  };

  setItems = items => {
    this._items = items;
  };

  setPricingRules = pricingRules => {
    this._pricingRules = pricingRules;
  };

  subTotal = () => {
    return Object.values(this._items).reduce(
      (carry, item) => parseFloat(carry + item.quantity * item.price),
      0,
    );
  };

  total = () => {
    const totalDiscounts = this.discounts().reduce(
      (carry, discount) => carry + discount.total,
      0,
    );

    return this.subTotal() - totalDiscounts;
  };

  itemsQuantity() {
    return Object.values(this._items).reduce(
      (carry, item) => parseFloat(carry + item.quantity),
      0,
    );
  }

  discounts = () => {
    return (
      this._items &&
      Object.keys(this._items).reduce((carry, key) => {
        const rule = this._pricingRules[key];

        if (!rule) return [...carry];

        const label = rule.label(this._items);
        const total = rule.total(this._items);

        return total === 0
          ? [...carry]
          : [
              ...carry,
              {
                id: rule.id,
                label,
                total: total && getFormattedPrice({ price: total }),
              },
            ];
      }, [])
    );
  };
}

export default Checkout;
