import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import App from 'App/App';

const initialState = {};
const mockStore = configureStore();
let store;

beforeEach(() => {
  store = mockStore(initialState);
});

describe('Modal', () => {
  it('should render correctly with no props', () => {
    const component = shallow(
      <Provider store={store}>
        <App />
      </Provider>,
    );

    expect(component).toMatchSnapshot();
  });
});
