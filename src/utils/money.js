import Dinero from 'dinero.js';
import {
  EUR_CURRENCY,
  DEFAULT_CURRENCY_FORMAT,
  DEFAULT_LOCALE,
} from '../constants';

export const createMoney = ({
  currency = EUR_CURRENCY,
  locale = DEFAULT_LOCALE,
  price,
}) => Dinero({ amount: price * 100, currency }).setLocale(locale);

export const getFormattedPrice = ({
  price,
  currency = EUR_CURRENCY,
  format = DEFAULT_CURRENCY_FORMAT,
}) => {
  if (!price) return 0;

  return createMoney({ price, currency }).toFormat(format);
};
