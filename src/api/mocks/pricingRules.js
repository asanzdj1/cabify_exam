const pricingRules = {
  MUG: {
    label: () => '2x1 Mug offer',
    id: '2-per-1',
    total: items => {
      const { quantity, price } = items.MUG;

      if (quantity < 2) return 0;

      return Math.floor(quantity / 2) * price;
    },
  },
  TSHIRT: {
    id: 'bulk',
    label: items => {
      const { quantity } = items.TSHIRT;

      return `x${quantity} Shirt offer`;
    },
    total: items => {
      const MIN_QUANTITY = 3;
      const PERCENT = 15;
      const { quantity, price } = items.TSHIRT;

      if (quantity < MIN_QUANTITY) return 0;

      return ((price * PERCENT) / 100) * quantity;
    },
  },
};

export default pricingRules;
