import React from 'react';
import { useSelector } from 'react-redux';

import Products from '../Products/Products';
import Summary from '../Summary/Summary';

const getFormattedProducts = (cart, products) => {
  if (products.length === 0) return [];

  return Object.keys(cart).map(key => ({
    ...cart[key],
    ...products.find(prod => prod.id === key),
  }));
};

const Cart = () => {
  const cartItems = useSelector(state => state.cart.items);
  const products = useSelector(state => state.product.products);

  const isLoading =
    Object.keys(cartItems).length === 0 && products.length === 0;

  return (
    !isLoading && (
      <div className="cart">
        <Products products={getFormattedProducts(cartItems, products)} />
        <Summary />
      </div>
    )
  );
};

export default Cart;
