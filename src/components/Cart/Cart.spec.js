import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import Cart from './Cart';

const initialState = {};
const mockStore = configureStore();
let store;

beforeEach(() => {
  store = mockStore(initialState);
});

describe('Cart', () => {
  it('should render correctly with no props', () => {
    const component = shallow(
      <Provider store={store}>
        <Cart />
      </Provider>,
    );

    expect(component).toMatchSnapshot();
  });
});
