import React from 'react';
import { shallow } from 'enzyme';

import NumberInput from './NumberInput';

const changeFn = jest.fn();

describe('NumberInput', () => {
  it('should render correctly with required props', () => {
    const component = shallow(
      <NumberInput name="inputName" onChange={changeFn} value={0} />,
    );

    expect(component).toMatchSnapshot();
  });

  it('should be emit change when + button is click', () => {
    const component = shallow(
      <NumberInput name="inputName" onChange={changeFn} value={0} />,
    );
    component.find('.symbol-plus').simulate('click');

    expect(changeFn).toHaveBeenCalled();
  });

  it('should be emit change when - button is click', () => {
    const component = shallow(
      <NumberInput name="inputName" onChange={changeFn} value={0} />,
    );
    component.find('.symbol-minus').simulate('click');

    expect(changeFn).toHaveBeenCalled();
  });

  it('should be emit value when input value changes', () => {
    const component = shallow(
      <NumberInput name="inputName" onChange={changeFn} value={0} />,
    );
    component.find('input[type="number"]').simulate('onChange');

    expect(changeFn).toHaveBeenCalled();
  });
});
