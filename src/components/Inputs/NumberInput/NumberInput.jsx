import React from 'react';
import { func, number, string } from 'prop-types';

const NumberInput = ({ name, onChange, value }) => {
  const handleChange = e => {
    onChange(e, parseFloat(e.target.value));
  };

  const handleAdd = e => {
    onChange(e, value + 1);
  };

  const handleRemove = e => {
    if (value === 0) return;
    onChange(e, value - 1);
  };

  return (
    <div className="number-input">
      <span className="symbol symbol-minus" onClick={handleRemove}>
        -
      </span>
      <input
        className="input"
        name={name}
        onChange={handleChange}
        type="number"
        value={value}
      />
      <span className="symbol symbol-plus" onClick={handleAdd}>
        +
      </span>
    </div>
  );
};

NumberInput.propTypes = {
  name: string.isRequired,
  onChange: func.isRequired,
  value: number,
};

NumberInput.defaultProps = {
  value: 0,
};

export default NumberInput;
