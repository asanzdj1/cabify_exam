import React from 'react';
import { useDispatch } from 'react-redux';
import { number, string } from 'prop-types';
import { useTranslation } from 'react-i18next';

import GlobalActions from 'store/modules/global';
import ProductActions from 'store/modules/product';
import CartActions from 'store/modules/cart';
import { getFormattedPrice } from 'utils/money';

import { PRODUCT } from '../../constants';
import { NumberInput } from '../Inputs';

const ItemLine = item => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleDetailClick = () => {
    dispatch(ProductActions.getProduct(item.id));
    dispatch(GlobalActions.openModal(`${PRODUCT}-${item.id}`));
  };

  const handleUpdateProduct = (e, value) => {
    dispatch(
      CartActions.updateItem({
        id: item.id,
        quantity: value,
        price: item.price,
      }),
    );
  };

  const productImage =
    item && item.image ? require(`../../assets/images/${item.image}`) : null;

  return (
    item && (
      <div className="product-line row">
        <div className="product-image col-detail" onClick={handleDetailClick}>
          {productImage && (
            <img src={productImage} alt={`product-${item.image}`} />
          )}
          <div className="description">
            <h3 className="heading-tertiary">{item.name}</h3>
            <p className="paragraph">
              {t('products/product-code', { code: item.code })}
            </p>
          </div>
        </div>
        <div className="col-quantity">
          <NumberInput
            value={item.quantity}
            onChange={handleUpdateProduct}
            name="quantity"
          />
        </div>
        <div className="product-price col-price">
          <span>{getFormattedPrice({ price: item.price })}</span>
        </div>
        <div className="product-total col-total">
          <span>{getFormattedPrice({ price: item.total })}</span>
        </div>
      </div>
    )
  );
};

ItemLine.propTypes = {
  quantity: number,
  code: string,
  id: string,
  image: string,
  name: string,
  price: number,
  total: number,
};

export default ItemLine;
