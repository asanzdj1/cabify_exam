import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import ItemLine from 'components/ItemLine/ItemLine';

const initialState = {};
const mockStore = configureStore();
let store;

beforeEach(() => {
  store = mockStore(initialState);
});

const item = {
  id: 'id',
  quantity: 1,
  price: 5,
};

describe('ItemLine', () => {
  it('should render correctly with no props', () => {
    const component = shallow(
      <Provider store={store}>
        <ItemLine item={item} />
      </Provider>,
    );

    expect(component).toMatchSnapshot();
  });
});
