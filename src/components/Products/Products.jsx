import React from 'react';
import { useTranslation } from 'react-i18next';
import { arrayOf, shape, string } from 'prop-types';

import ItemLine from '../ItemLine';

const Products = ({ products }) => {
  const { t } = useTranslation();

  const headerData = [
    {
      id: 'detail',
      title: t('cart/product-details'),
    },
    {
      id: 'quantity',
      title: t('cart/quantity'),
    },
    {
      id: 'price',
      title: t('cart/price'),
    },
    {
      id: 'total',
      title: t('cart/total'),
    },
  ];

  return (
    <div className="products">
      <h2 className="heading-primary">{t('cart/shopping-cart')}</h2>
      <ul className="products-labels">
        <li className="row">
          {headerData.map(({ id, title }) => (
            <div className={`col-${id}`} key={id}>
              {title}
            </div>
          ))}
        </li>
        <ul className="products-list">
          {products.map(product => {
            return <ItemLine key={product.id} {...product} />;
          })}
        </ul>
      </ul>
    </div>
  );
};

Products.propTypes = {
  products: arrayOf(
    shape({
      id: string,
    }),
  ),
};

Products.defaultProps = {
  products: [],
};

export default Products;
