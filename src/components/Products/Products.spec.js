import React from 'react';
import { shallow } from 'enzyme';

import Products from './Products';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: key => key }),
}));

describe('Products', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<Products />);

    expect(component).toMatchSnapshot();
  });
});
