import React from 'react';
import { func, bool, string, oneOfType } from 'prop-types';

const Button = ({ disabled, children, onClick }) => (
  <button
    onClick={onClick}
    className={`button ${disabled && 'button--disabled'}`}
    type="button"
  >
    {children}
  </button>
);

Button.propTypes = {
  children: oneOfType([func, string]),
  disabled: bool,
  onClick: func,
};

Button.defaultProps = {
  children: () => {},
  disabled: false,
  onClick: () => {},
};

export default Button;
