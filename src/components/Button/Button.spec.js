import React from 'react';
import { shallow } from 'enzyme';

import Button from './Button';

const clickFn = jest.fn();

describe('Button', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<Button>mock</Button>);

    expect(component).toMatchSnapshot();
  });

  it('should render with disabled class', () => {
    const component = shallow(<Button disabled>mock</Button>);

    expect(component).toMatchSnapshot();
  });

  it('should be clickable', () => {
    const component = shallow(<Button onClick={clickFn}>mock</Button>);
    component.simulate('click');

    expect(clickFn).toHaveBeenCalled();
  });
});
