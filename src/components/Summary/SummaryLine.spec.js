import React from 'react';
import { shallow } from 'enzyme';

import SummaryLine from './SummaryLine';

describe('SummaryLine', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<SummaryLine />);

    expect(component).toMatchSnapshot();
  });

  it('should render correctly with values', () => {
    const component = shallow(<SummaryLine label="Label" value="value" />);

    expect(component).toMatchSnapshot();
  });
});
