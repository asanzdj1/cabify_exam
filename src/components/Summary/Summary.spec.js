import React from 'react';
import { shallow } from 'enzyme';

import Summary from './Summary';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: key => key }),
}));

describe('Summary', () => {
  it('should render correctly with no props', () => {
    const component = shallow(<Summary />);

    expect(component).toMatchSnapshot();
  });
});
