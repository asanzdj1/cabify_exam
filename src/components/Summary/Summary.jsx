import React, { useEffect } from 'react';
import { array, number, string } from 'prop-types';
import { useTranslation } from 'react-i18next';

import co from 'services/Checkout';
import { getFormattedPrice } from 'utils/money';
import pricingRules from 'api/mocks/pricingRules';

import SummaryLine from './SummaryLine';
import Button from '../Button';

const Summary = () => {
  useEffect(() => {
    co.setPricingRules(pricingRules);
  }, []);

  const { t } = useTranslation();

  return (
    <div className="summary">
      <h2 className="heading-primary">{t('summary/order-summary')}</h2>
      <ul className="summary-items">
        <SummaryLine
          label={`${co.itemsQuantity()} ${t('summary/items')}`}
          value={getFormattedPrice({ price: co.subTotal() })}
        />
      </ul>
      <h3 className="heading-secondary">{t('summary/discounts')}</h3>
      <ul className="summary-discounts">
        {co.discounts() &&
          co.discounts().map(discount => {
            return (
              <SummaryLine
                key={discount.id}
                label={discount.label}
                value={discount.total}
              />
            );
          })}
        <SummaryLine label="Promo code" value={0} />
      </ul>
      <div className="summary-total">
        <div className="total">
          <span className="total-cost">{t('summary/total-cost')}</span>
          <span className="total-price">
            {getFormattedPrice({ price: co.total() })}
          </span>
        </div>
        <Button>{t('summary/checkout')}</Button>
      </div>
    </div>
  );
};

Summary.propTypes = {
  discounts: array,
  items: array,
  promoCode: string,
  total: number,
};

Summary.defaultProps = {
  discounts: [],
  items: [],
  promoCode: null,
  total: 0,
};

export default Summary;
