import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import Modal from './Modal';

const initialState = {};
const mockStore = configureStore();
let store;

beforeEach(() => {
  store = mockStore(initialState);
});

describe('Modal', () => {
  it('should render correctly with no props', () => {
    const component = shallow(
      <Provider store={store}>
        <Modal>mock</Modal>
      </Provider>,
    );

    expect(component).toMatchSnapshot();
  });

  it('should render correctly with open prop', () => {
    const component = shallow(
      <Provider store={store}>
        <Modal open>mock</Modal>
      </Provider>,
    );

    expect(component).toMatchSnapshot();
  });
});
