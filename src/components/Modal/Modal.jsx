import React from 'react';
import { bool, func, object, oneOfType, string } from 'prop-types';
import { useDispatch } from 'react-redux';

import GlobalActions from 'store/modules/global';

const Modal = ({ children, open = false }) => {
  const dispatch = useDispatch();
  const handleCloseModal = () => dispatch(GlobalActions.closeModal());

  return (
    <div className={`modal ${open && 'modal--opened'}`}>
      <div className="modal-wrapper">
        <i className="icon-arrows-remove closer" onClick={handleCloseModal} />
        {children}
      </div>
    </div>
  );
};

Modal.propTypes = {
  children: oneOfType([func, object, string]),
  open: bool,
};

Modal.defaultProps = {
  children: () => {},
  open: false,
};

export default Modal;
