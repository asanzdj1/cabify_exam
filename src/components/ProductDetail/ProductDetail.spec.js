import React from 'react';
import { shallow } from 'enzyme';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import ProductDetail from './ProductDetail';

const initialState = {};
const mockStore = configureStore();
let store;

beforeEach(() => {
  store = mockStore(initialState);
});

describe('ProductDetail', () => {
  it('should render correctly with no props', () => {
    const component = shallow(
      <Provider store={store}>
        <ProductDetail />
      </Provider>,
    );

    expect(component).toMatchSnapshot();
  });
});
