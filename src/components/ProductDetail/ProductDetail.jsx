import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import CartActions from 'store/modules/cart';

import { PRODUCT } from '../../constants';
import Button from '../Button';
import Modal from '../Modal';

const ProductDetail = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const modalId = useSelector(state => state.global.modal.open);
  const product = useSelector(state => state.product.product);

  const isModalOpen = modalId === `${PRODUCT}-${product.id}`;

  const handleAdd = () => {
    dispatch(CartActions.addItem(product.id));
  };

  return (
    Object.keys(product).length !== 0 && (
      <Modal open={isModalOpen}>
        <div className="product-detail">
          <img
            alt={`product-${product.name}`}
            className="product-image"
            src={require(`../../assets/images/${product.imageDetail}`)}
          />
          <div className="product-description">
            <h2 className="title heading-primary">
              <span>{product.name}</span>
              <span>{product.price}</span>
            </h2>
            <p className="description">{product.description}</p>
            <p className="paragraph product-code">
              <span>{t('product-detail/product-code')}</span>
              <span> {product.code}</span>
            </p>
            <Button onClick={handleAdd}>
              {t('product-detail/add-to-cart')}
            </Button>
          </div>
        </div>
      </Modal>
    )
  );
};

export default ProductDetail;
