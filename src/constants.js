export const PRODUCT = 'PRODUCT';

// currencies
export const EUR_CURRENCY = 'EUR';
export const DEFAULT_LOCALE = 'es-ES';
export const DEFAULT_CURRENCY_FORMAT = '$0';
