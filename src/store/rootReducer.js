import { combineReducers } from 'redux';

import { reducer as cart } from './modules/cart';
import { reducer as global } from './modules/global';
import { reducer as product } from './modules/product';

const rootReducer = combineReducers({
  cart,
  global,
  product,
});

export default rootReducer;
