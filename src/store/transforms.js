import { createTransform } from 'redux-persist';

import co from 'services/Checkout';
import pricingRules from 'api/mocks/pricingRules';

const cartTransform = createTransform(
  // transform state on its way to being serialized and persisted.
  inboundState => {
    return { ...inboundState };
  },
  // transform state being rehydrated
  (outboundState, key) => {
    if (key === 'cart') {
      co.setItems(outboundState.items);
      co.setPricingRules(pricingRules);
    }

    return { ...outboundState };
  },
  // define which reducers this transform gets called for.
  { whitelist: ['cart'] },
);

export default cartTransform;
