import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import localStorage from 'redux-persist/lib/storage';
import cartTransform from './transforms';

import rootReducer from './rootReducer';

const rootPersistConfig = {
  key: 'root',
  storage: localStorage,
  whitelist: ['cart'],
  transforms: [cartTransform],
};

const configureStore = () => {
  const enhancers = composeWithDevTools();
  const persistedReducer = persistReducer(rootPersistConfig, rootReducer);

  const store = createStore(persistedReducer, enhancers);
  const persistor = persistStore(store);

  return { store, persistor };
};

export default configureStore;
