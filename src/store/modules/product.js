import { createActions, createReducer } from 'reduxsauce';

import products from 'api/mocks/products.json';
import co from 'services/Checkout';

export const PREFIX = 'PRODUCT_';
export const initialState = {
  products: [],
  product: {},
};

// Actions and action types
const { Types: ProductTypes, Creators } = createActions(
  {
    getProducts: null,
    getProduct: ['id'],
  },
  {
    prefix: PREFIX,
  },
);

// Reducers
const handlers = {
  [ProductTypes.GET_PRODUCTS]: (state = initialState) => {
    co.setProducts(products);

    return {
      ...state,
      products,
    };
  },
  [ProductTypes.GET_PRODUCT]: (state = initialState, { id }) => ({
    ...state,
    product: products.find(item => item.id === id),
  }),
};

const reducer = createReducer(initialState, handlers);

export { ProductTypes, reducer };
export default Creators;
