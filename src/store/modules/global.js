import { createActions, createReducer } from 'reduxsauce';

export const PREFIX = 'GLOBAL_';
export const initialState = {
  modal: {
    open: null,
  },
};

// Actions and action types
const { Types: GlobalTypes, Creators } = createActions(
  {
    openModal: ['id'],
    closeModal: null,
  },
  {
    prefix: PREFIX,
  },
);

// Reducers
const handlers = {
  [GlobalTypes.OPEN_MODAL]: (state = initialState, { id }) => ({
    ...state,
    modal: { ...state.modal, open: id },
  }),
  [GlobalTypes.CLOSE_MODAL]: (state = initialState) => ({
    ...state,
    modal: { ...state.modal, open: null },
  }),
};

const reducer = createReducer(initialState, handlers);

export { GlobalTypes, reducer };
export default Creators;
