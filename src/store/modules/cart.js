import { createActions, createReducer } from 'reduxsauce';

import cart from 'api/mocks/cart.json';
import co from 'services/Checkout';

export const PREFIX = 'CART_';
export const initialState = {
  items: { ...cart },
  discounts: [],
};

// Actions and action types
const { Types: CartTypes, Creators } = createActions(
  {
    addItem: ['id'],
    getCart: null,
    getPricingRules: null,
    updateItem: ['item'],
  },
  {
    prefix: PREFIX,
  },
);

// Reducers
const handlers = {
  [CartTypes.GET_CART]: (state = initialState) => {
    return {
      ...state,
      items: {
        ...cart,
      },
    };
  },
  [CartTypes.UPDATE_ITEM]: (state = initialState, { item }) => {
    const { id, price, quantity } = item;

    co.scan(id, quantity);

    return {
      ...state,
      items: {
        ...state.items,
        [id]: { quantity, price, total: quantity * price },
      },
    };
  },
  [CartTypes.ADD_ITEM]: (state = initialState, { id }) => {
    const newQuantity = state.items[id].quantity + 1;
    const total = newQuantity * state.items[id].price;

    co.scan(id);

    return {
      ...state,
      items: {
        ...state.items,
        [id]: { ...state.items[id], quantity: newQuantity, total },
      },
    };
  },
};

const reducer = createReducer(initialState, handlers);

export { CartTypes, reducer };
export default Creators;
