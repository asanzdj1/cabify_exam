import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

// TODO: Find a right solution: https://github.com/facebook/react/issues/15865
const useFetching = fetchActionCreator => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchActionCreator);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []); // Want it to act like ComponentDidMount
};

export default useFetching;
