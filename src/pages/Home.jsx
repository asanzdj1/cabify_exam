import React from 'react';

import ProductActions from '../store/modules/product';
import { Cart } from '../components';
import ProductDetail from '../components/ProductDetail';
import useFetching from '../hooks/useFetching';

const Home = () => {
  useFetching(ProductActions.getProducts());

  return (
    <div className="home">
      <ProductDetail />
      <Cart />
    </div>
  );
};

export default Home;
